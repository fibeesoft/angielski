﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TaskManager : MonoBehaviour
{
    SpellingsArray script_spellingsArray;
    [SerializeField] GameObject go_btn;
    int taskNumber;
    string taskName;

    void Start()
    {
        script_spellingsArray = GameObject.Find("Canvas").GetComponent<SpellingsArray>();
        taskNumber = 0;
        Spellings();
    }

    public void Spellings()
    {
        taskName = script_spellingsArray.spellingsArray[taskNumber];
        GameObject newButton = GameObject.Instantiate(go_btn, new Vector2(100f,0f), Quaternion.identity, GameObject.FindGameObjectWithTag("TaskContainer").transform);
        newButton.transform.localPosition = new Vector2(0, 0);
        newButton.name = taskName;
        taskNumber++;
        print(taskName);

        taskName = script_spellingsArray.spellingsArray[taskNumber];
        GameObject newButton2 = GameObject.Instantiate(go_btn, new Vector2(100f, 0f), Quaternion.identity, GameObject.FindGameObjectWithTag("TaskContainer").transform);
        newButton2.transform.localPosition = new Vector2(400, 0);
        newButton2.name = taskName;
        print(taskName);
    }


}
