﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskAnswerFullQuestion : MonoBehaviour {

    //The third task

    public class AnswerQuestionClass
    {
        public string imageName;
        public string soundQuestionName;
        public string soundAnswerName;
        public string question;
        public string good_answer;
        public string answer2;
        public string answer3;

        public AnswerQuestionClass(string imgName, string sndQuestionName, string sndAnswerName, string quest, string good_answ, string answ2, string answ3)
        {
            imageName = imgName;
            soundQuestionName = sndQuestionName;
            soundAnswerName = sndAnswerName;
            question = quest;
            good_answer = good_answ;
            answer2 = answ2;
            answer3 = answ3;
        }
    }

    public static TaskAnswerFullQuestion instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public Button btn_img_question, btn_txt_question;
    public Button[] btns_txt_answer;
    public List<AnswerQuestionClass> fullQuestionList = new List<AnswerQuestionClass>();
    public string clickedButtonName, taskName;
    public int taskNr;

    void Start()
    {
        fullQuestionList.Add(new AnswerQuestionClass("apple", "", "", "What is it?", "It is an apple.", "It is a peach.", "It is a banana." ));
        fullQuestionList.Add(new AnswerQuestionClass("banana", "", "", "What is it?", "It is a banana.", "It is a peach.", "It is a apple."));
        fullQuestionList.Add(new AnswerQuestionClass("orange", "", "", "What is it?", "It is an orange.", "It is a peach.", "It is a apple."));
        fullQuestionList.Add(new AnswerQuestionClass("five", "", "", "What number is it?", "It's number five.", "It's number eight.", "It's number four."));
        fullQuestionList.Add(new AnswerQuestionClass("brown", "", "", "What colour is it?", "It's brown.", "It's red.", "It's blue."));
        fullQuestionList.Add(new AnswerQuestionClass("Monday", "", "", "What's the first day of the week?", "It's Monday.", "It's Saturday.", "It's Wednesday."));
        fullQuestionList.Add(new AnswerQuestionClass("dots", "", "", "How many blue dots are there?", "There are four, blue dots.", "There are four, green dots.", "There are five, blue dots."));
        fullQuestionList.Add(new AnswerQuestionClass("dots", "", "", "How many red dots are there?", "There are three, red dots.", "There are four, red dots.", "There are two, red dots."));
        fullQuestionList.Add(new AnswerQuestionClass("two_red_pencils", "", "", "How many red pencils are there?", "There are two, red pencils.", "There are two, blue pencils.", "There are four, red pencils."));
        fullQuestionList.Add(new AnswerQuestionClass("pencils", "", "", "How many green pencils are there?", "There are four, green pencils.", "There are three, green pencils.", "There are four, red pencils."));
    }

    public void GetAnswerFullQuestion()
    {
        taskNr = Random.Range(0, fullQuestionList.Count);
        btn_img_question.GetComponent<Button>().image.sprite = (Sprite)Resources.Load("img/" + fullQuestionList[taskNr].imageName, typeof(Sprite));
        btn_txt_question.GetComponentInChildren<Text>().text = fullQuestionList[taskNr].question;
        btns_txt_answer[0].GetComponentInChildren<Text>().text = fullQuestionList[taskNr].good_answer;
        btns_txt_answer[1].GetComponentInChildren<Text>().text = fullQuestionList[taskNr].answer2;
        btns_txt_answer[2].GetComponentInChildren<Text>().text = fullQuestionList[taskNr].answer3;
        reshuffle(btns_txt_answer);
        taskName = fullQuestionList[taskNr].good_answer;
    }

    public void Check()
    {
        if (clickedButtonName == taskName)
        {
            PointsManager.instance.AddPoint();
            GetAnswerFullQuestion();
        }
    }

    public void GetClickedButtonName(string clickedBtnName)
    {
        clickedButtonName = clickedBtnName;
    }

    void reshuffle(Button[] btns_txt_answer)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < btns_txt_answer.Length; t++)
        {
            Button tmp = btns_txt_answer[t];
            int r = Random.Range(t, btns_txt_answer.Length);
            btns_txt_answer[t] = btns_txt_answer[r];
            btns_txt_answer[r] = tmp;
        }
    }
}