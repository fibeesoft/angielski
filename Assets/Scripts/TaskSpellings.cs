﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TaskSpellings : MonoBehaviour
{
    //the fourth task
    public static TaskSpellings instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public Text txt_podpowiedzSpellings;
    public Image img_grafikaSpellings;
    public AudioSource audio_zadanieZListyString;
    public InputField odpowiedzSpellings;
    public GameObject polePodpowiedzi;
    bool isSpellingCorrect;
    public Button btn_sound, btn_podpowiedz, btn_sprawdz;
    public static int SpellingsMode;
    Lista skryptZlista;
    int iloscWszystkichSlowekZSpellingsList;
    int aktualnyNrZadaniaZSpellingsList;
    bool isAnsweredGoodAtFirstTime;

    void Start()
    {
        skryptZlista = GameObject.Find("GameManager").GetComponent<Lista>();
        iloscWszystkichSlowekZSpellingsList = skryptZlista.spellingsList.Count;
        Button btn_soundOnClick = btn_sound.GetComponent<Button>();
        btn_soundOnClick.onClick.AddListener(OdtworzAudioZadanieZListyString);
        Button btn_podpowiedzOnClick = btn_podpowiedz.GetComponent<Button>();
        btn_podpowiedzOnClick.onClick.AddListener(PokazPodpowiedz);
        Button btn_sprawdzOnClick = btn_sprawdz.GetComponent<Button>();
        btn_sprawdzOnClick.onClick.AddListener(CheckIfSpellingCorrect);
        btn_sprawdzOnClick.onClick.AddListener(GetNextSpelling);
        isSpellingCorrect = false;
        SpellingsMode = 3;
        aktualnyNrZadaniaZSpellingsList = 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            CheckIfSpellingCorrect();
            GetNextSpelling();
        }
    }

    public void GetSpelling()
    {
        GameManager.instance.ChangeTaskMode(1);
        isSpellingCorrect = false;
        
        if(SpellingsMode == 3)
        {
            aktualnyNrZadaniaZSpellingsList = UnityEngine.Random.Range(0, iloscWszystkichSlowekZSpellingsList);
            txt_podpowiedzSpellings.text = skryptZlista.spellingsList[aktualnyNrZadaniaZSpellingsList];
            string pathImageResource = "img/" + txt_podpowiedzSpellings.text;
            img_grafikaSpellings.sprite = (Sprite)Resources.Load(pathImageResource, typeof(Sprite));
            OdtworzAudioZadanieZListyString();
            polePodpowiedzi.SetActive(true);
            print("aktualny nr zadania = " + aktualnyNrZadaniaZSpellingsList);
        }
        else if (SpellingsMode == 4)
        {
            txt_podpowiedzSpellings.text = skryptZlista.spellingsList[aktualnyNrZadaniaZSpellingsList];
            string pathImageResource = "img/" + txt_podpowiedzSpellings.text;
            img_grafikaSpellings.sprite = (Sprite)Resources.Load(pathImageResource, typeof(Sprite));
            OdtworzAudioZadanieZListyString();
            polePodpowiedzi.SetActive(false);
            print("aktualny nr zadania = " + aktualnyNrZadaniaZSpellingsList);
        }
    }

    public void UpdateAktualnyNrZadaniaZSpellingsList(int aktualnyNrZadaniaZSpellingsListy)
    {
        aktualnyNrZadaniaZSpellingsList = aktualnyNrZadaniaZSpellingsListy;
    }

    void OdtworzAudioZadanieZListyString()
    {
        string pathAudioResource = "audio/" + txt_podpowiedzSpellings.text;
        audio_zadanieZListyString.clip = (AudioClip)Resources.Load(pathAudioResource, typeof(AudioClip));
        audio_zadanieZListyString.Play();
        odpowiedzSpellings.ActivateInputField();
    }

    void PokazPodpowiedz()
    {
        polePodpowiedzi.SetActive(true);
        odpowiedzSpellings.ActivateInputField();
        isAnsweredGoodAtFirstTime = false;
    }

    public void GetNextSpelling()
    {
        if (isSpellingCorrect == true && SpellingsMode == 3)
        {
            SpellingsMode = 4;
            GetSpelling();
            odpowiedzSpellings.text = "";  
        }else if (isSpellingCorrect == true && SpellingsMode == 4)
        {
            SpellingsMode = 3;
            GetSpelling();
            odpowiedzSpellings.text = "";
        }
    }

    public void CheckIfSpellingCorrect()
    {
        if(odpowiedzSpellings.text == skryptZlista.spellingsList[aktualnyNrZadaniaZSpellingsList])
        {
            isSpellingCorrect = true;
            PointsManager.instance.AddPoint();
        }
        else
        {
            print("Zła odpowiedz");
            odpowiedzSpellings.ActivateInputField();
        }
    }

    public void ZwiekszNrZadaniaZListy()
    {
        aktualnyNrZadaniaZSpellingsList++;
    }


    public int PobierzCalkowitaIloscSlowek()
    {
        return iloscWszystkichSlowekZSpellingsList;
    }
}
