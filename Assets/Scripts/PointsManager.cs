﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour
{

    public static PointsManager instance = null;

    int lessonPointCounter, levelCounter;
    public Slider slider_level, slider_lesson;
    int thisLessonAllTasksNumber;
    public GameObject plazmaEffect;
    public Text txt_levelCounter;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        thisLessonAllTasksNumber = 10;
        lessonPointCounter = 0;
        levelCounter = 0;
        slider_level.maxValue = 10;
        slider_level.value = 0;
        slider_lesson.maxValue = thisLessonAllTasksNumber;
        slider_lesson.value = 0;
    }

    public void AddPoint()
    {
        lessonPointCounter++;
        UpdatePointsBar();
    }

    public void UpdatePointsBar()
    {
        if(lessonPointCounter <= thisLessonAllTasksNumber)
        {
            slider_lesson.value++;
        }
        else
        {
            NextLevel();
            lessonPointCounter = 0;
            slider_lesson.value = 0;
        }
    }

    public void NextLevel()
    {
        levelCounter++;
        slider_level.value++;
        txt_levelCounter.text = levelCounter.ToString();
        Effects(0f, 0f, plazmaEffect);
    }

    public void Effects(float xPosition, float yPosition, GameObject effectName)
    {
        GameObject efektPlazmy = Instantiate(effectName, new Vector3(xPosition, yPosition, 6f), Quaternion.identity) as GameObject;
        Destroy(efektPlazmy, 2);
    }
}
