﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskChooseImage : MonoBehaviour {

    //the first task
    public static TaskChooseImage instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    int iloscWszystkichSlowekZSpellingsList;
    public GameObject empty_chooseImage;
    public Button [] obrazkiDoWyboru = new Button[4];
    int[] losoweNumery = new int[4];
    string[] obrazkiDoWyboruNazwa = new string[4];
    public Text nazwaZadania;
    string pobranaNazwaPrzycisku;
    public AudioSource audio_zadanieZListyString;
    public Button btn_zadanie;
    Lista skryptZlista;

    private void Start()
    {
        skryptZlista = GameObject.Find("GameManager").GetComponent<Lista>();
        iloscWszystkichSlowekZSpellingsList = skryptZlista.spellingsList.Count;
    }

    private void Update()
    {
        CheckChooseImage();
    }

    public void GetChooseImage()
    {
        for (int i=0; i<obrazkiDoWyboru.Length; i++)
        {
            obrazkiDoWyboru[i].GetComponent<Outline>().effectColor = new Color(0f, 0f, 255f);
        }
        losoweNumery[0] = Random.Range(0, iloscWszystkichSlowekZSpellingsList);
        losoweNumery[1] = Random.Range(0, iloscWszystkichSlowekZSpellingsList);
        losoweNumery[2] = Random.Range(0, iloscWszystkichSlowekZSpellingsList);
        losoweNumery[3] = Random.Range(0, iloscWszystkichSlowekZSpellingsList);
        obrazkiDoWyboru[0].GetComponent<Image>().sprite = (Sprite)Resources.Load("img/" + skryptZlista.spellingsList[losoweNumery[0]], typeof(Sprite));
        obrazkiDoWyboru[1].GetComponent<Image>().sprite = (Sprite)Resources.Load("img/" + skryptZlista.spellingsList[losoweNumery[1]], typeof(Sprite));
        obrazkiDoWyboru[2].GetComponent<Image>().sprite = (Sprite)Resources.Load("img/" + skryptZlista.spellingsList[losoweNumery[2]], typeof(Sprite));
        obrazkiDoWyboru[3].GetComponent<Image>().sprite = (Sprite)Resources.Load("img/" + skryptZlista.spellingsList[losoweNumery[3]], typeof(Sprite));
        obrazkiDoWyboruNazwa[0] = obrazkiDoWyboru[0].GetComponent<Image>().sprite.name;
        obrazkiDoWyboruNazwa[1] = obrazkiDoWyboru[1].GetComponent<Image>().sprite.name;
        obrazkiDoWyboruNazwa[2] = obrazkiDoWyboru[2].GetComponent<Image>().sprite.name;
        obrazkiDoWyboruNazwa[3] = obrazkiDoWyboru[2].GetComponent<Image>().sprite.name;
        reshuffle(obrazkiDoWyboruNazwa);
        nazwaZadania.text = obrazkiDoWyboruNazwa[0];
        btn_zadanie.GetComponentInChildren<Text>().text = obrazkiDoWyboruNazwa[0];
        OdtworzAudioZadanieZListyString();
    }

    public void OdtworzAudioZadanieZListyString()
    {
        string pathAudioResource = "audio/" + nazwaZadania.text;
        audio_zadanieZListyString.clip = (AudioClip)Resources.Load(pathAudioResource, typeof(AudioClip));
        audio_zadanieZListyString.Play();
    }

    public string GetNazwaZadania()
    {
        string nazwaAktualnegoZadania = nazwaZadania.text;
        return nazwaAktualnegoZadania;
    }

    public void GetAClickedButtonName(string kliknietyButton)
    {
        pobranaNazwaPrzycisku = kliknietyButton;  
    }

    public void CheckChooseImage()
    {
        if(pobranaNazwaPrzycisku == nazwaZadania.text)
        {
            PointsManager.instance.AddPoint();
            GetChooseImage();
        }
    }

    void reshuffle(string[] obrazkiDoWyboruNazwa)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < obrazkiDoWyboruNazwa.Length; t++)
        {
            string tmp = obrazkiDoWyboruNazwa[t];
            int r = Random.Range(t, obrazkiDoWyboruNazwa.Length);
            obrazkiDoWyboruNazwa[t] = obrazkiDoWyboruNazwa[r];
            obrazkiDoWyboruNazwa[r] = tmp;
        }
    }
}
