﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public GameObject mainMenu, Spelling, ChooseImage, WriteAnswer, FiveTasksMenu, ChooseTextAnswer, AnswerFullQuestion, fiveMenuEmpty;
    int taskMode, whichTaskIsBeingDone;
    Lista skryptZlista;
    bool isMenuOn;

    private void Start()
    {
        whichTaskIsBeingDone = 0;
        taskMode = 0;
        skryptZlista = GameObject.Find("GameManager").GetComponent<Lista>();
        isMenuOn = true;
        fiveMenuEmpty.SetActive(true);
    }

    public void ChangeWhichTaskIsBeingDone(int task)
    {
        whichTaskIsBeingDone = task;
    }

    public void ChangeTaskMode(int taskModeNumber)
    {
        taskMode = taskModeNumber;
    }

    public void QuitTheGame()
    {
        Application.Quit();
    }

    public void GetChooseImage()
    {
        DeactivateAllTasks();
        ChangeWhichTaskIsBeingDone(1);
        ChooseImage.SetActive(true);
        TaskChooseImage.instance.GetChooseImage();
    }

    public void GetChooseTextAnswer()
    {
        DeactivateAllTasks();
        ChangeWhichTaskIsBeingDone(2);
        ChooseTextAnswer.SetActive(true);
        TaskChooseTextAnswer.instance.ChooseTextAnswer();
    }

    public void GetAnswerFullQuestion()
    {
        DeactivateAllTasks();
        ChangeWhichTaskIsBeingDone(3);
        AnswerFullQuestion.SetActive(true);
        TaskAnswerFullQuestion.instance.GetAnswerFullQuestion();
    }

    public void GetSpellings3()
    {
        DeactivateAllTasks();
        ChangeWhichTaskIsBeingDone(4);
        Spelling.SetActive(true);
        TaskSpellings.SpellingsMode = 3;
        TaskSpellings.instance.GetSpelling();
    }
    
    public void GetWriteAnswer()
    {
        DeactivateAllTasks();
        ChangeWhichTaskIsBeingDone(5);
        WriteAnswer.SetActive(true);
        TaskWriteAnswer.instance.GetWriteAnswer();
    }

    public void DeactivateAllTasks()
    {
        Spelling.SetActive(false);
        ChooseImage.SetActive(false);
        WriteAnswer.SetActive(false);
        ChooseTextAnswer.SetActive(false);
        AnswerFullQuestion.SetActive(false);
    }

    public void OpenCloseFiveMenu()
    {
        if (isMenuOn == true)
        {
            fiveMenuEmpty.SetActive(false);
            isMenuOn = false;
        }
        else
        {
            fiveMenuEmpty.SetActive(true);
            isMenuOn = true;
        }
    }

    public void OnclickButtonAction()
    {
        OpenCloseFiveMenu();
    }

    public void AddPoint()
    {
        PointsManager.instance.AddPoint();
    }

    public void linkFunc()
    {
        Application.OpenURL("http://zombiaki.pl/index.php");
    }

    public void OpenWebsite()
    {
        Application.OpenURL("http://www.zombiaki.pl/");
    }
}
