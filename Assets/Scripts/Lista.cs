﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lista : MonoBehaviour {

    public List<string> spellingsList = new List<string>();

    private void Awake()
    {

        //Lesson1
        spellingsList.Add("book");
        spellingsList.Add("car");
        spellingsList.Add("box");
        spellingsList.Add("pencil");
        spellingsList.Add("red");
        spellingsList.Add("green");
        spellingsList.Add("blue");
        //Lesson2
        spellingsList.Add("bike");
        spellingsList.Add("house");
        spellingsList.Add("chair");
        spellingsList.Add("table");
        spellingsList.Add("pink");
        spellingsList.Add("yellow");
        spellingsList.Add("orange");
        //Lesson3
        spellingsList.Add("cat");
        spellingsList.Add("dog");
        spellingsList.Add("carrot");
        spellingsList.Add("ball");
        spellingsList.Add("one");
        spellingsList.Add("two");
        spellingsList.Add("three");
        //Lesson4
        spellingsList.Add("apple");
        spellingsList.Add("orange");
        spellingsList.Add("banana");
        spellingsList.Add("bag");
        spellingsList.Add("four");
        spellingsList.Add("five");
        spellingsList.Add("six");
        //Lesson5
        spellingsList.Add("cup");
        spellingsList.Add("glass");
        spellingsList.Add("pen");
        spellingsList.Add("seven");
        spellingsList.Add("eight");
        spellingsList.Add("nine");
        spellingsList.Add("ten");
        //Lesson6
        spellingsList.Add("water");
        spellingsList.Add("bread");
        spellingsList.Add("milk");
        spellingsList.Add("candy");
        spellingsList.Add("cookie");
        spellingsList.Add("plum");
        spellingsList.Add("juice");
        //Lesson7
        spellingsList.Add("bird");
        spellingsList.Add("chicken");
        spellingsList.Add("fish");
        spellingsList.Add("horse");
        spellingsList.Add("tea");
        spellingsList.Add("coffee");
        spellingsList.Add("cocoa");

        spellingsList.Add("big");
        spellingsList.Add("small");
        spellingsList.Add("road");
        spellingsList.Add("snake");
        spellingsList.Add("tree");
        spellingsList.Add("bus");
        spellingsList.Add("lemon");

        //lesson8
        spellingsList.Add("star");
        spellingsList.Add("black");
        spellingsList.Add("sun");
        spellingsList.Add("moon");
        spellingsList.Add("spoon");
        spellingsList.Add("shark");
        spellingsList.Add("turtle");
        //lesson9
        spellingsList.Add("long");
        spellingsList.Add("short");
        spellingsList.Add("hand");
        spellingsList.Add("leg");
        spellingsList.Add("eye");
        spellingsList.Add("brown");
        spellingsList.Add("grey");

        //lesson10
        spellingsList.Add("clock");
        spellingsList.Add("door");
        spellingsList.Add("rain");
        spellingsList.Add("pot");
        spellingsList.Add("purple");
        spellingsList.Add("white");
        spellingsList.Add("paint");

        ////lesson11
        //spellingsList.Add("brush");
        //spellingsList.Add("hat");
        //spellingsList.Add("shoes");
        //spellingsList.Add("tomato");
        //spellingsList.Add("cow");
        //spellingsList.Add("rabbit");
        //spellingsList.Add("horse");

        ////lesson12
        //spellingsList.Add("knife");
        //spellingsList.Add("sad");
        //spellingsList.Add("happy");
        //spellingsList.Add("soup");
        //spellingsList.Add("chicken");
        //spellingsList.Add("pear");
        //spellingsList.Add("doll");

        ////lesson13
        //spellingsList.Add("socks");
        //spellingsList.Add("bottle");   
        //spellingsList.Add("egg");
        //spellingsList.Add("window");
        //spellingsList.Add("hot");
        //spellingsList.Add("cold");
        //spellingsList.Add("flower");

        ////lesson14
        //spellingsList.Add("grass");
        //spellingsList.Add("ant");
        //spellingsList.Add("plate");
        //spellingsList.Add("sugar");
        //spellingsList.Add("sand");
        //spellingsList.Add("river");
        //spellingsList.Add("ladybird");

        ////lesson15
        //spellingsList.Add("paper");
        //spellingsList.Add("soap");
        //spellingsList.Add("towel");
        //spellingsList.Add("teeth");
        //spellingsList.Add("wash");
        //spellingsList.Add("clean");
        //spellingsList.Add("dirty");

        ////lesson16
        //spellingsList.Add("light");
        //spellingsList.Add("butter");
        //spellingsList.Add("kettle");
        //spellingsList.Add("sandwich");
        //spellingsList.Add("cheese");
        //spellingsList.Add("ham");
        //spellingsList.Add("onion");

        ////lesson17
        //spellingsList.Add("grey");
        //spellingsList.Add("bed");
        //spellingsList.Add("pillow");
        //spellingsList.Add("bin");
        //spellingsList.Add("fruits");
        //spellingsList.Add("shelf");
        //spellingsList.Add("carpet");

        ////lesson18
        //spellingsList.Add("pepper");
        //spellingsList.Add("sweetcorn");
        //spellingsList.Add("fork");
        //spellingsList.Add("pasta");
        //spellingsList.Add("potato");
        //spellingsList.Add("bucket");
        //spellingsList.Add("nuts");

        //spellingsList.Add("key");
        //spellingsList.Add("train");
        //spellingsList.Add("string");
        //spellingsList.Add("ring");
        //spellingsList.Add("rainbow");
        //spellingsList.Add("bridge");
        //spellingsList.Add("circle");
        

        //spellingsList.Add("eleven");
        //spellingsList.Add("twelve");
        //spellingsList.Add("thirteen");
        //spellingsList.Add("fourteen");
        //spellingsList.Add("fifteen");
        //spellingsList.Add("letter");
        //spellingsList.Add("parcel");

        //spellingsList.Add("sixteen");
        //spellingsList.Add("seventeen");
        //spellingsList.Add("eighteen");
        //spellingsList.Add("nineteen");
        //spellingsList.Add("twenty");
        //spellingsList.Add("grapes");
        //spellingsList.Add("chips");
       

        //spellingsList.Add("Monday");
        //spellingsList.Add("Tuesday");
        //spellingsList.Add("Wednesday");
        //spellingsList.Add("Thursday");
        //spellingsList.Add("Friday");
        //spellingsList.Add("Saturday");
        //spellingsList.Add("Sunday");

        //spellingsList.Add("strawberry");
        //spellingsList.Add("blueberry");
        //spellingsList.Add("raspberry");
        //spellingsList.Add("coconut");
        //spellingsList.Add("tiger");
        //spellingsList.Add("lion");
        //spellingsList.Add("zebra");

        //spellingsList.Add("peach");
        //spellingsList.Add("bowl");
        //spellingsList.Add("cake");
        //spellingsList.Add("gift");
        //spellingsList.Add("balloon");
        

    }
}
