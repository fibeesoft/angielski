﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TaskChooseTextAnswer : MonoBehaviour {

    //the second task
	public static TaskChooseTextAnswer instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    Lista skryptZlista;
    public Button imageButton;
    public Button[] tablicaOdpowiedziTekstowych;
    int[] tablicaLosowychZadan = new int[3];
    string clickedButtonName;
    string taskName;

	void Start () {
		skryptZlista = GameObject.Find("GameManager").GetComponent<Lista>();
	}
	
    public void ChooseTextAnswer()
    {
        tablicaLosowychZadan[0] = Random.Range(0, skryptZlista.spellingsList.Count);
        tablicaLosowychZadan[1] = Random.Range(0, skryptZlista.spellingsList.Count);
        tablicaLosowychZadan[2] = Random.Range(0, skryptZlista.spellingsList.Count);
        tablicaOdpowiedziTekstowych[0].GetComponentInChildren<Text>().text = skryptZlista.spellingsList[tablicaLosowychZadan[0]];
        tablicaOdpowiedziTekstowych[1].GetComponentInChildren<Text>().text = skryptZlista.spellingsList[tablicaLosowychZadan[1]];
        tablicaOdpowiedziTekstowych[2].GetComponentInChildren<Text>().text = skryptZlista.spellingsList[tablicaLosowychZadan[2]];
        taskName = skryptZlista.spellingsList[tablicaLosowychZadan[0]];
        reshuffle(tablicaOdpowiedziTekstowych);
        imageButton.image.sprite = (Sprite)Resources.Load("img/" + skryptZlista.spellingsList[tablicaLosowychZadan[0]], typeof(Sprite));
    }

    public void Check()
    {
        if(clickedButtonName == taskName)
        {
            PointsManager.instance.AddPoint();
            ChooseTextAnswer();
        }
    }

    public void GetClickedButtonName(string clickedBtnName)
    {
        clickedButtonName = clickedBtnName;
    }

    void reshuffle(Button[] tablicaOdpowiedziTekstowych)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < tablicaOdpowiedziTekstowych.Length; t++)
        {
            Button tmp = tablicaOdpowiedziTekstowych[t];
            int r = Random.Range(t, tablicaOdpowiedziTekstowych.Length);
            tablicaOdpowiedziTekstowych[t] = tablicaOdpowiedziTekstowych[r];
            tablicaOdpowiedziTekstowych[r] = tmp;
        }
    }
}
