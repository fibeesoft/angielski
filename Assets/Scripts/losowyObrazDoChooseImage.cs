﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class losowyObrazDoChooseImage : MonoBehaviour {

    public string nameOfTheSprite;
    public Button btn_object;

    public void GetClickedButtonName()
    {
        nameOfTheSprite = btn_object.GetComponent<Image>().sprite.name;
        TaskChooseImage.instance.GetAClickedButtonName(nameOfTheSprite);

        print(nameOfTheSprite);
        string aktualneZadanie = TaskChooseImage.instance.GetNazwaZadania();
        if (nameOfTheSprite != aktualneZadanie)
        {
            GetARedBorder();
        }
    }

    public void GetARedBorder()
    {
        btn_object.GetComponent<Outline>().effectColor = new Color(255f, 0f, 0f);
    }
}
