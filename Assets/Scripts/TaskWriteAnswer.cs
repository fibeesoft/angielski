﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskWriteAnswer : MonoBehaviour {

    public static TaskWriteAnswer instance = null;

    public class WriteAnswer
    {
        public string imageName;
        public string audioName;
        public string pytanieAngielski;
        public string pytaniePolski;
        public string odpowiedzAngielski;
        public string odpowiedzPolski;

        public WriteAnswer(string imgName, string audName, string pytAng, string pytPol, string odpAng, string odpPol)
        {
            pytanieAngielski = pytAng;
            pytaniePolski = pytPol;
            odpowiedzAngielski = odpAng;
            odpowiedzPolski = odpPol;
            imageName = imgName;
            audioName = audName;
        }
    }

    public List<WriteAnswer> qa = new List<WriteAnswer>();
    
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public Text txt_pytanieAngielskie, txt_odpowiedzAngielska, txt_pytaniePolskie, txt_odpowiedzPolska;
    public Image img_zdjecieGlowne;
    public InputField inp_odpowiedz;
    public int writeAnswerTaskNo;

    public void GetWriteAnswer()
    {
        int losoweZadanie = Random.Range(0, qa.Count);
        writeAnswerTaskNo = losoweZadanie;
        txt_pytaniePolskie.text = "";
        txt_odpowiedzAngielska.text = "";
        inp_odpowiedz.text = "";
        inp_odpowiedz.ActivateInputField();
        txt_pytanieAngielskie.text = qa[writeAnswerTaskNo].pytanieAngielski;
        txt_odpowiedzPolska.text = qa[writeAnswerTaskNo].odpowiedzPolski;
        LoadImage();
    }

    public void PokazOdpowiedz()
    {
        txt_odpowiedzAngielska.text = qa[writeAnswerTaskNo].odpowiedzAngielski;
        inp_odpowiedz.ActivateInputField();
    }

    private void Start()
    {
        writeAnswerTaskNo = 0;
        qa.Add(new WriteAnswer("house", "", "What is it?", "Co to jest?", "It is a house.", "To jest dom"));
        qa.Add(new WriteAnswer("blue_pencil", "", "What is this?", "Co to jest?", "This is a blue pencil.", "To jest niebieski ołówek."));
        qa.Add(new WriteAnswer("green_pencil", "", "What is this?", "Co to jest?", "This is a green pencil.", "To jest zielony ołówek."));
        qa.Add(new WriteAnswer("rainbow", "", "What is that?", "Co to jest?", "That is a rainbow.", "To jest tęcza."));
        qa.Add(new WriteAnswer("apple", "", "What is this?", "Co to jest?", "This is an apple.", "To jest jabłko."));
        qa.Add(new WriteAnswer("shark", "", "What is it?", "Co to jest?", "It is a shark.", "To jest rekin."));
        qa.Add(new WriteAnswer("horse", "", "What is this?", "Co to jest?", "This is a horse.", "To jest koń."));
        qa.Add(new WriteAnswer("box", "", "What is it?", "Co to jest?", "It is a box.", "To jest pudełko."));
        qa.Add(new WriteAnswer("brown", "", "What colour is it?", "Jaki to jest kolor?", "It is a brown colour.", "To jest kolor brązowy."));
        qa.Add(new WriteAnswer("red", "", "What colour is it?", "Jaki to jest kolor?", "It is a red colour.", "To jest kolor czerwony."));
        qa.Add(new WriteAnswer("blue", "", "What colour is it?", "Jaki to jest kolor?", "It is a blue colour.", "To jest kolor niebieski."));
        qa.Add(new WriteAnswer("yellow", "", "What colour is it?", "Jaki to jest kolor?", "It is a yellow colour.", "To jest kolor żółty."));
        qa.Add(new WriteAnswer("pencil", "", "What do you have?", "Co Ty masz?", "I have a pencil.", "Ja mam ołówek."));
        qa.Add(new WriteAnswer("flower", "", "What do you have?", "Co Ty masz?", "I have a flower.", "Ja mam kwiatka."));
        qa.Add(new WriteAnswer("ball", "", "What do you have?", "Co Ty masz?", "I have a ball.", "Ja mam piłkę."));
        qa.Add(new WriteAnswer("bike", "", "What do you have?", "Co Ty masz?", "I have a bike.", "Ja mam rower."));
        qa.Add(new WriteAnswer("book", "", "What do you have?", "Co Ty masz?", "I have a red book.", "Ja mam czerwoną książkę."));
        qa.Add(new WriteAnswer("cat", "", "What do you have?", "Co Ty masz?", "I have a little cat.", "Ja mam małego kotka."));
        qa.Add(new WriteAnswer("cookie", "", "What do you like?", "Co Ty lubisz?", "I like cookies.", "Ja lubię ciastka."));
        qa.Add(new WriteAnswer("juice", "", "What do you like?", "Co Ty lubisz?", "I like orange juice.", "Ja lubię sok pomarańczowy."));
        qa.Add(new WriteAnswer("fruits", "", "What do you like?", "Co Ty lubisz?", "I like fruits.", "Ja lubię owoce."));
        qa.Add(new WriteAnswer("carrot", "", "What do you like?", "Co Ty lubisz?", "I like carrots.", "Ja lubię marchewki."));
        qa.Add(new WriteAnswer("apple", "", "What do you like?", "Co Ty lubisz?", "I like apples.", "Ja lubię jabłka."));
        qa.Add(new WriteAnswer("cookie", "", "Do you like cookies?", "Czy Ty lubisz ciastka?", "Yes, I do.", "Tak, lubię."));
        qa.Add(new WriteAnswer("bread", "", "Do you like bread?", "Czy Ty lubisz chleb?", "No, I don't.", "Nie, nie lubię."));
        qa.Add(new WriteAnswer("chips", "", "Do you like chips?", "Czy Ty lubisz frytki?", "Yes, I do.", "Tak, lubię."));
        qa.Add(new WriteAnswer("cheese", "", "Do you like cheese?", "Czy Ty lubisz ser?", "No, I don't.", "Nie, nie lubię."));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            CheckTheAnswer();
        }
    }

    public void LoadImage()
    {
        img_zdjecieGlowne.sprite = (Sprite)Resources.Load("img/" + qa[writeAnswerTaskNo].imageName, typeof(Sprite));
    }

    public void ShowTranslation()
    {
        inp_odpowiedz.ActivateInputField();
        txt_pytaniePolskie.text = qa[writeAnswerTaskNo].pytaniePolski;

    }

    public void CheckTheAnswer()
    {
        if(inp_odpowiedz.text == qa[writeAnswerTaskNo].odpowiedzAngielski)
        {
            if(writeAnswerTaskNo < qa.Count)
            {
                PointsManager.instance.AddPoint();
                writeAnswerTaskNo++;
                GetWriteAnswer();
            }
            else
            {
                PointsManager.instance.AddPoint();
                writeAnswerTaskNo = 0;
                GetWriteAnswer();
            }

        }
    }
}
