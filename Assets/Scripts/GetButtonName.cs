﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetButtonName : MonoBehaviour {

    string buttonName;
    public Button btn_object;

    public void GetButtonsName()
    {
        buttonName = btn_object.GetComponentInChildren<Text>().text;
        print(buttonName);
        TaskChooseTextAnswer.instance.GetClickedButtonName(buttonName);
        TaskChooseTextAnswer.instance.Check();
        TaskAnswerFullQuestion.instance.GetClickedButtonName(buttonName);
        TaskAnswerFullQuestion.instance.Check();
    }
}
